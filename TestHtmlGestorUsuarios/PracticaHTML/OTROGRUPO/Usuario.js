
class Usuario{
           
    constructor(nombre,edad,altura,act){
        
        if((typeof(nombre) !== "string") ||(typeof(edad) !== "number") ||(typeof(altura) !== "number") ||(typeof(act) !== "boolean"))
            throw "Argumento no valido";

        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura; 
        this.act = act;   
    }

}
