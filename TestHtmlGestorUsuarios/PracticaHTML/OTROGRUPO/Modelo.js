var arrayUsuarios;
class Modelo {

    constructor() {
        this.OnCreate = null;
        this.OnUpdate = null;
        this.OnDelete = null;
        this.datosIncorrectos = null;
        this.datosCorrectos = null;

    }
    comprobarTipo(usuario) {
        if (typeof (usuario.nombre) === "string" && !usuario.nombre == "" && isNaN(parseInt(usuario.nombre))) {
            if (!isNaN(parseInt(usuario.edad))) {
                if (!isNaN(parseFloat(usuario.altura))) {
                    return true;
                }
            }
        }
        else
            return false;
    }
    leer() {
        arrayUsuarios = JSON.parse(window.localStorage.getItem("lista-usuarios"));
        if (arrayUsuarios !== null)
            this.OnCreate(arrayUsuarios);
        else
            arrayUsuarios = [];
    }

    guardarDatosModelo(arrayUsuarios) {
        window.localStorage.setItem("lista-usuarios", JSON.stringify(arrayUsuarios));
    }

    crear(usuario) {
        if(this.comprobarTipo(usuario)) {
            arrayUsuarios.push(usuario);
            this.guardarDatosModelo(arrayUsuarios);
            this.OnCreate(arrayUsuarios);
            this.datosCorrectos();
        } else {
            this.datosIncorrectos();
        }
    }

    modificar(usuariomodificar, nuevousuario) {
        if (this.comprobarTipo(nuevousuario)) {
            arrayUsuarios[arrayUsuarios.indexOf(usuariomodificar)] = nuevousuario;
            this.guardarDatosModelo(arrayUsuarios);
            this.OnUpdate(arrayUsuarios);
            this.datosCorrectos();
        } else {
            this.datosIncorrectos();
            this.OnUpdate(arrayUsuarios);

        }
    }



    eliminar(usuarioeliminar) {
        arrayUsuarios.splice(arrayUsuarios.indexOf(usuarioeliminar), 1);
        this.guardarDatosModelo(arrayUsuarios);
        this.OnDelete(arrayUsuarios);
    }

    /*
    validarUsuario(){
        let campoNombre = document.getElementById("nombre");    
        let nombre = campoNombre.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
        if (nombre == "") {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca un nombre";
            return null;
        }
        let edad = parseInt(document.getElementById("edad").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(edad)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una edad correcta";
            return null;
        }
        let altura = parseFloat(document.getElementById("altura").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(altura)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una altura";
            return null;
        }
        let activo = document.getElementById("activo").checked;
        document.getElementById("mensaje-validacion").className = "no_ver";
        return new Usuario(nombre,edad,altura,activo);
    }*/
}