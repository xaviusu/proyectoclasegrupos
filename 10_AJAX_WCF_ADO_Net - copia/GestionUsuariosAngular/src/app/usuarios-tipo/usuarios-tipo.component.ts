import { Component, OnInit } from '@angular/core';
import { ServicioGestionUsuariosService } from '../servicio-gestion-usuarios.service';

@Component({
  selector: 'app-usuarios-tipo',
  templateUrl: './usuarios-tipo.component.html',
  styleUrls: ['./usuarios-tipo.component.css']
})
export class UsuariosTipoComponent implements OnInit {

  constructor(public srvGestUsu : ServicioGestionUsuariosService) { }

  ngOnInit(): void {
  }

}
