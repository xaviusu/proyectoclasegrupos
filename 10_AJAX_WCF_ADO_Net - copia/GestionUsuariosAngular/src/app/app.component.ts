import { Component } from '@angular/core';
import { ServicioGestionUsuariosService } from './servicio-gestion-usuarios.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'GestionUsuariosAngular';
  constructor(public svrEj : ServicioGestionUsuariosService) {

  }
}
