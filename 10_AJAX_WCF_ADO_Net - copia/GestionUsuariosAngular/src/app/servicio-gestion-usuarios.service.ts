import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario';
import { Observable } from 'rxjs';
import { UsuariosEstadoComponent } from './usuarios-estado/usuarios-estado.component';

@Injectable({
  providedIn: 'root'
})
export class ServicioGestionUsuariosService {
  private arrayUsuarios : Array<Usuario> = [];
  private url : string = "http://localhost:8635/usuarios"
  constructor(private clienteHTTP : HttpClient ) {
    let observ: Observable<Array<Usuario>>;
    observ = this.clienteHTTP.get<Array<Usuario>>(this.url);
    observ.subscribe((datos : Array<Usuario>) => {
      this.arrayUsuarios = datos;
      this.calcularEstadooTipo(0);

    });
   }

   public usuarios() : Array<Usuario> {
    return this.arrayUsuarios;
   }

   public calcularEstadooTipo(estadoOTipo : number) : Array<number> {
    let primerValor : number = 0;
    let segundoValor : number = 0;
    let arrayEstadosyTipos : Array<number> = [];
    for(let i = 0; i < this.arrayUsuarios.length; i++) {
      if(estadoOTipo === 0) {
        if(this.arrayUsuarios[i].activo) {
          primerValor++;
        } else {
          segundoValor++;
        }
      } else {
        if(this.arrayUsuarios[i].edad >= 18) {
          primerValor++;
        } else {
          segundoValor++;
        }
      }
    }
    arrayEstadosyTipos.push(primerValor);
    arrayEstadosyTipos.push(segundoValor);
    
    return arrayEstadosyTipos;
   }

}
