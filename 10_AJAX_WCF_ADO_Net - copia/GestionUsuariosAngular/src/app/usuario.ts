export class Usuario {

    public nombre: string = "";
    public edad : number = 0;
    public altura : number = 0;
    public id : number = 0;
    public activo : boolean = false;

}
