using NUnit.Framework;
using ModeloUsuarios;
using System;
// Ante la duda es mejor separar en funciones, crear test v�lidos y test inv�lidos
namespace TestModeloUsuario
{
    enum Entorno
    {
        Ninguno = 0,
        Produccion = 1,
        Desarrollo = 2,
        Preproduccion = 3,

    }
    public class TestsControladorYModeloUsuarios
    {
        ModeloUsuario modelo;
        Controlador controlador;
        Entorno entorno;
        ModuloPersistencia moduloPersistencia;
        const string DIR_DATA = "Gestion de usuarios";
        [SetUp]
        public void Setup()
        {
            string path = "C:/Users/pmpcurso1/Desktop/hola.json";
            System.IO.File.WriteAllText(path, "");
            moduloPersistencia = new ModuloPersistencia();
            modelo = ModeloUsuario.Instancia;
            controlador = new Controlador(modelo);  
        }
           
        [Test]
        public void TestCrear()
        {
            Usuario usu1 = new Usuario("hola", 32, 1.6f);
            controlador.Crear(usu1);
            controlador.Crear(new Usuario("adios", -3, 2.2f));
            controlador.Crear(new Usuario("qtal", 400, 27));
            Assert.AreEqual(1, controlador.LeerUno(1).Edad);
            Assert.AreEqual(1.0f, controlador.LeerUno(2).Altura);
            Assert.Null(controlador.LeerUno(4));
            Assert.AreEqual(controlador.LeerTodos().Count, 3, "HOLA");
            Assert.AreEqual(controlador.LeerUno(0), usu1);
        }
   
        [Test]
        public void TestModificar()
        {
            Usuario usu1 = new Usuario("hola", 27, 1.5f);
            controlador.Crear(usu1);
            controlador.Crear(new Usuario("adios", 33, 2));
            controlador.Crear(new Usuario("qtal", 45, 2));

            Usuario usu1mod = new Usuario("hola", -3, 7);//Te lo cambia aqu� directamente
            controlador.Modificar(usu1mod);
            Assert.AreNotEqual(controlador.LeerUno(0), usu1);
            Assert.AreEqual(controlador.LeerUno(0), usu1mod);
            Usuario usuNoExiste = new Usuario("NoExisto",88 ,88 );
            controlador.Modificar(usuNoExiste);
            for(int i=0;i< controlador.LeerTodos().Count; i++)
            {
                Assert.AreNotEqual(controlador.LeerUno(i), usuNoExiste);
            }
        }

        [Test]
        public void TestEliminar()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            controlador.Crear(usu1);
            controlador.Crear(new Usuario("adios", 43, 2));
            controlador.Crear(new Usuario("qtal", 40, 2));
            Usuario usu = controlador.LeerTodos()[1];
            Assert.AreEqual(true, controlador.Eliminar(1));
            foreach(Usuario usuario in controlador.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu);
            }
            Assert.AreEqual(2, controlador.LeerTodos().Count);
            Assert.AreEqual(false, controlador.Eliminar(5)); 
        }

        [Test]
        public void TestLeerUno()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            controlador.Crear(usu1);
            controlador.Crear(new Usuario("adios", 43, 2));
            controlador.Crear(new Usuario("qtal", 40, 2));
            Assert.Null(controlador.LeerUno(4));
            Assert.NotNull(controlador.LeerUno(1));
            Assert.IsInstanceOf<Usuario>(controlador.LeerUno(1)); 
        }
        
        [Test]
        public void TestLeerTodos()
        {
            Assert.AreEqual(0, controlador.LeerTodos().Count);
            controlador.Crear(new Usuario("adios", 43, 2));
            controlador.Crear(new Usuario("qtal", 40, 2));
            Assert.AreEqual(2, controlador.LeerTodos().Count);
            controlador.Eliminar(0);
            Assert.AreEqual(1, controlador.LeerTodos().Count);
        }
    }
}