﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    interface IVista
    {
        void Menu();

        void Crear();

        bool Eliminar(int entero);

        public Usuario LeerUno(int entero);

        public IList<Usuario> LeerTodos();
    }
}
