﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{

    public class ModuloPersistencia
    {
        //ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();

        public enum Entorno
        {
            Ninguno = 0,
            Produccion = 1,
            Desarrollo = 2,
            Preproduccion = 3
        }
        const string DIR_DATA = "GestionUsuarios";
        Entorno entorno;

        public ModuloPersistencia()
        {
            
            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            listaUsuarios = Leer();
        }
        //public void Guardar(List<Usuario> usus)
        public void Crear(Usuario usuario)
        {
            if(!LeerConfirmar(usuario.GetNombre())) // LeerConfirmar devuelve true si existe el usuario
            {
                listaUsuarios.Add(usuario);
                Guardar(listaUsuarios);
            }
        }
        
        public  void Guardar(List<Usuario> usus)
        {
            string str = "";
            foreach(Usuario usu in usus)
            {
                str += JsonSerializer.Serialize<Usuario>(usu) + '\n';
            }
            string path = "C:/Users/pmpcurso1/Desktop/hola.json";
            System.IO.File.WriteAllText(path, str);
        }


        public bool Eliminar(int entero)
        {
            if(listaUsuarios.Count > entero)
            {
                listaUsuarios.RemoveAt(entero);
                Guardar(listaUsuarios);
                return true;
            } else
            {
                return false;
            }
        }
        public  List<Usuario> Leer()
        {
            Usuario usu;
            string path = "C:/Users/pmpcurso1/Desktop/hola.json";
            int counter = 0;
            string line = "";
            System.IO.StreamReader file = null;
            // Read the file and display it line by line.  
            try { 
                file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    usu = JsonSerializer.Deserialize<Usuario>(line);
                    listaUsuarios.Add(usu);
                    counter++;
                }
                file.Close();
            } catch
            {

            }
            
            return listaUsuarios;
        }

        public IList<Usuario> LeerTodo()
        {
            return listaUsuarios;
        }

        public Usuario LeerUno(int entero)
        {
            if(listaUsuarios.Count > entero)
            {
                return listaUsuarios[entero];
            } else
            {
                return null;
            }
        }

        public bool LeerConfirmar(string nombre) // devuelve true si existe en usuario
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public void Modificar(Usuario usu)
        {
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == usu.GetNombre())
                {
                    listaUsuarios[i] = usu;
                }
            }
            Guardar(listaUsuarios);
        }
    }
}
