﻿ using System;

namespace Ejemplo01_Singleton
{
    class Modulo1
    {
        public static void Main2(string[] args)
        {
            GestorTextos gt = GestorTextos.GetInstancia();
            gt.Nuevo("EEEE");
            gt.Nuevo("FFFF");
            gt.Nuevo("GGGG");
            gt.Mostrar();
        }
    }
}
