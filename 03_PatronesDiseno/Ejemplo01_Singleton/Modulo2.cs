﻿using System;

namespace Ejemplo01_Singleton
{
    class Modulo2
    {
        public static void Main(string[] args)
        {
            //GestorTextos gt = new GestorTextos(); Da error
            GestorTextos.GetInstancia().Nuevo("AAAA");
            GestorTextos.GetInstancia().Nuevo("BBBB");
            GestorTextos.GetInstancia().Nuevo("CCCC");
            GestorTextos.GetInstancia().Nuevo("DDDD");
            Modulo1.Main2(null);

        }
    }
}
