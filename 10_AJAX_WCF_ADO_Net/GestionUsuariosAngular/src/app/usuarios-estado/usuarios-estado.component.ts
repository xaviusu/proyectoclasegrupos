import { Component, OnInit } from '@angular/core';
import { ServicioGestionUsuariosService } from '../servicio-gestion-usuarios.service';
import { Usuario } from '../usuario';
@Component({
  selector: 'app-usuarios-estado',
  templateUrl: './usuarios-estado.component.html',
  styleUrls: ['./usuarios-estado.component.css']
})
export class UsuariosEstadoComponent implements OnInit {

  
  constructor(public srvGestUsu : ServicioGestionUsuariosService) {
    
   }

  ngOnInit(): void {
  }

}
