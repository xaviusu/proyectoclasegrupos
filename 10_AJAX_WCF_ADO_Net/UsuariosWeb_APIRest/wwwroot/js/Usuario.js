
class Usuario{
           
    constructor(nombre,edad,altura,activo,id = null){
        
        if((typeof(nombre) !== "string") ||(typeof(edad) !== "number") ||(typeof(altura) !== "number") ||(typeof(activo) !== "boolean"))
            throw "Argumento no valido";
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura; 
        this.activo = activo;   
    }

    
}
