using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace TestHtmlGestorUsuarios
{
    public class Tests
    {
        IWebDriver driver;
        IWebElement campoNombre;
        IWebElement campoEdad;
        IWebElement campoAltura;
        IWebElement campoActivo;
        IWebElement btnAnadir;
        IWebElement btnModificar;
        Process _process;
        int filasAlInicio;
        [OneTimeSetUp]
        public void InicializarClaseTest()
        {

            string fichFirefox = "../../../../FirefoxPortable/FirefoxPortable.exe";
            // Se comprueba si el usuario tiene la versi�n portable de firefox instalada, sino la instala
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            //Se configura el driver de selenium para Firefox
            if (File.Exists(fichFirefox))
            {
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = "../../../../FirefoxPortable/App/Firefox64/firefox.exe";
                FirefoxDriverService geckoservice = FirefoxDriverService.CreateDefaultService("./");
                geckoservice.Host = "::1";
                driver = new FirefoxDriver(geckoservice, firefoxOptions);
            }
            Assert.NotNull(driver, "Driver null");
            StartServer();
            Wait(5, 2, TipoWait.PorTiempo);

            AbrirPagWeb();
            Wait(3, 2, TipoWait.PorTiempo);

            filasAlInicio = driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count; //coge la �ltima posici�n del usuario

        }
        private void StartServer()
        {

            _process = new Process
            {
                StartInfo =
                {
                    FileName = @"dotnet.exe",
                    //Arguments = $@"run {applicationPath}\{projectName}.csproj"
                    Arguments = "run --project ..\\..\\..\\..\\..\\UsuariosWeb_APIRest\\UsuariosWeb_APIRest.csproj"
                }
            };
            _process.Start();
        }
        // Se abre la p�gina web
        public void AbrirPagWeb()
        {
            string path = "http://127.0.0.1:8635/Vista.html";
            driver.Navigate().GoToUrl(path);

        }

        [SetUp]
        public void IniciarVariables()
        {
            //Se inicializan los campos a rellenar
            campoNombre = driver.FindElement(By.Id("nombre"));
            campoEdad = driver.FindElement(By.Id("edad"));
            campoAltura = driver.FindElement(By.Id("altura"));
            campoActivo = driver.FindElement(By.Id("activo"));
            btnAnadir = driver.FindElement(By.Id("btn-anadir"));
        }
        //Se esperan 5 segundos y se cierra el navegador
        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            Wait(5, 2, TipoWait.PorTiempo);
            driver.Close();
            _process.Kill();
            _process.Dispose();
            _process.Close();
        }

        //Elimina todos los usuarios uno a uno para terminar los test
        [TearDown]
        public void EliminarUsuarios()
        {

            int numFilas = driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count;
            for (int i = numFilas; i > filasAlInicio; i--)
            {
                IWebElement btnEliminar = driver.FindElement(By.XPath("//*[@id='btn-eliminar" + (i - 1) + "']"));
                btnEliminar.Click();
                
            }
        }

        [Test] 
        public void TestConsultaDatos()
        {
            string pathConsulta = "http://127.0.0.1:8635";
            Wait(2, 2, TipoWait.PorTiempo);
            driver.Navigate().GoToUrl(pathConsulta);
            driver.FindElement(By.XPath("/html/body/div/main/div/a[2]")).Click();
            Wait(2, 2, TipoWait.PorTiempo);
            Assert.AreEqual(2, int.Parse(driver.FindElement(By.XPath("/html/body/app-root/app-usuarios-estado/table/tbody/tr/td[1]")).Text));
            Assert.AreEqual(4, int.Parse(driver.FindElement(By.XPath("/html/body/app-root/app-usuarios-estado/table/tbody/tr/td[2]")).Text));
            Assert.AreEqual(4, int.Parse(driver.FindElement(By.XPath("/html/body/app-root/app-usuarios-tipo/table/tbody/tr/td[1]")).Text));
            Assert.AreEqual(2, int.Parse(driver.FindElement(By.XPath("/html/body/app-root/app-usuarios-tipo/table/tbody/tr/td[2]")).Text));
            Wait(4, 2, TipoWait.PorTiempo);
            driver.FindElement(By.XPath("/html/body/app-root/a")).Click();
            string pathVuelta = "http://127.0.0.1:8635/Vista.html";
            Wait(7, 2, TipoWait.PorTiempo);
            driver.Navigate().GoToUrl(pathVuelta);
        }

        [Test, Order(4)]
        public void TestComprobarLS()
        {
            //Se crean numUsuarios usuarios validos 
            Random rand = new Random();
            int numUsuarios = rand.Next(2, 10);
            CrearUsuarios(numUsuarios);
            //Se refresca la p�gina 
            driver.Navigate().Refresh();
            Wait(1, 2, TipoWait.PorTiempo);
            //Se comprueba que el n�mero de filas coincida con el n�mero de usuarios creados
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + numUsuarios, "No coincide el numero de filas");
        }

        [Test]
        public void TestCrear()
        {
            //Creacion primer usuario, se espera a q se a�ada y se comprueban las filas
            RellenarDatos("USUARIO1", "23", "2", true);
            Wait(5, filasAlInicio + 1, TipoWait.PorTabla);
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + 1, "No coincide el numero de filas");
            ComprobarUsuario("USUARIO1", "23", "2", true, filasAlInicio + 1);
            //Segundo usuario
            RellenarDatos("USUARIO2", "24", "1.6", true);
            Wait(5, filasAlInicio + 2, TipoWait.PorTabla);
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + 2, "No coincide el numero de filas");
            ComprobarUsuario("USUARIO2", "24", "1.6", true, filasAlInicio + 2);
            // Creacion usuario sin parametros para comprobar que no se crea
            RellenarDatos("", "", "", false);
            Wait(5, 2, TipoWait.PorMensaje);
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + 2, "No coincide el numero de filas");

        }

        public void ComprobarUsuario(string nombre, string edad, string altura, bool activo, int fila)
        {
            //Se comprueba parametro a parametro si el usuario es el mismo
            Assert.AreEqual(driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + fila + "]/td[1]")).Text, nombre, "No coincide el nombre de la fila " + fila);
            Assert.AreEqual(driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + fila + "]/td[2]")).Text, edad, "No coincide la edad de la fila " + fila);
            Assert.AreEqual(driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + fila + "]/td[3]")).Text, altura, "No coincide la altura de la fila " + fila);
            Assert.AreEqual(bool.Parse(driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + fila + "]/td[4]")).Text), activo, "No coincide el activo de la fila " + fila);
        }

        [Test]
        public void TestModificar()
        {
            //Creacion primer usuario, se espera a q se a�ada y se comprueban que los datos sean lso mismos
            RellenarDatos("USUARIO1", "23", "2", true);
            Wait(5, filasAlInicio + 1, TipoWait.PorTabla);
            ComprobarUsuario("USUARIO1", "23", "2", true, filasAlInicio + 1);
            //Se busca el boton modificar
            btnModificar = driver.FindElement(By.Id("btn-modificar" + filasAlInicio));
            Assert.NotNull(btnModificar, "Boton modificar null");
            btnModificar.Click();
            //Se clica modificar y se comprueba que se actualiza el formulario
            WaitForCampos(2, "USUARIO1", "23", "2", true, filasAlInicio + 1);
            ComprobarUsuario(campoNombre.GetProperty("value"), campoEdad.GetProperty("value"), campoAltura.GetProperty("value"), bool.Parse(campoActivo.GetProperty("checked")), filasAlInicio + 1);
            RellenarDatos("USUARIOMOD", "23", "1.1", false);
            //Se modifica el usuario y se comprueba q se haya cambiado correctamente
            int filaTabla = 1 + filasAlInicio;
            string nombreTabla = driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[1]")).Text;
            WaitForNombre(5, "USUARIOMOD", filaTabla);
            Assert.AreEqual(nombreTabla, "USUARIOMOD", "Usuario modificado no coincide");
            ComprobarUsuario("USUARIOMOD", "23", "1.1", false, filasAlInicio + 1);
            //Se modifica el usuario por uno invalido y se comprueba que los datos no cambian
            btnModificar = driver.FindElement(By.Id("btn-modificar0"));
            btnModificar.Click();
            WaitForCampos(2, "USUARIOMOD", "23", "1.1", false, 1);
            ComprobarUsuario(campoNombre.GetProperty("value"), campoEdad.GetProperty("value"), campoAltura.GetProperty("value"), bool.Parse(campoActivo.GetProperty("checked")), 1);
            RellenarDatos("", "23", "1.1", false);
            Wait(5, 1, TipoWait.PorMensaje);
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + 1, "No coincide el numero de filas");
            ComprobarUsuario("USUARIOMOD", "23", "1.1", false, 1 + filasAlInicio);

        }

        [Test]
        public void TestEliminar()
        {
            //Generamos un n�mero aleatorio de usuarios con datos aleatorios
            Random rand = new Random();
            int numUsuarios = rand.Next(2, 10);
            CrearUsuarios(numUsuarios);
            //Eliminamos el primer usuario y comprobamos que el numero de filas ha bajado en uno
            IWebElement btnEliminar = driver.FindElement(By.XPath("//*[@id='btn-eliminar"+(filasAlInicio + 1)+"']"));
            btnEliminar.Click();
            
            Assert.AreEqual(driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count, filasAlInicio + numUsuarios - 1, "No coincide el numero de filas");
            
        }

        public void CrearUsuarios(int numUsuarios)
        {
            //Creacion numUsuarios con datos aleatorios validos
            Random rand = new Random();

            for (int i = 0; i < numUsuarios; i++)
            {
                int edad = rand.Next(1, 100);
                double altura = Math.Round(rand.NextDouble() * (3 - 1) + 1, 2);
                bool act = rand.Next(0, 2) == 1;
                RellenarDatos("USUARIO" + i, edad.ToString(), altura.ToString().Replace(",", "."), act);
                ComprobarUsuario("USUARIO" + i, edad.ToString(), altura.ToString().Replace(",", "."), act, filasAlInicio + i + 1);
            }
        }

        public void RellenarDatos(string nombre, string edad, string altura, bool activo)
        {
            //Borramos cada campo y reescribimos en �l
            campoNombre.Clear();
            campoNombre.SendKeys(nombre);
            campoEdad.Clear();
            campoEdad.SendKeys(edad);
            campoAltura.Clear();
            campoAltura.SendKeys(altura);
            // Se comprueba si esta clicado o no y se pulsa dependiendo de su estado
            if (bool.Parse(campoActivo.GetProperty("checked")))
            {
                if (!activo)
                {
                    campoActivo.Click();
                }
            }
            else
            {
                if (activo)
                {
                    campoActivo.Click();
                }
            }
            //Se pulsa el bot�n a�adir y se a�ade
            btnAnadir.Click();
        }
        //Enum para esperar dependiendo de la funci�n
        public enum TipoWait
        {
            PorTabla,
            PorMensaje,
            PorTiempo
        }
        // Funci�n que espera hasta que se cumpla una condici�n o hasta que haya pasado el timeOut
        public void Wait(int seg, int numFilas, TipoWait tipoWait, int timeOut = 5)
        {

            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 0, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeIni = DateTime.Now;
            if (tipoWait == TipoWait.PorTabla) // Se espera hasta que se actualice la tabla
            {
                try
                {
                    wait.Until(webDriver => driver.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count == numFilas);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
            else if (tipoWait == TipoWait.PorMensaje)//Se espera hasta que aparezca el mensaje
            {
                try
                {
                    wait.Until(webDriver => driver.FindElement(By.XPath("//*[@id='mensError']")).Displayed == true);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
            else if (tipoWait == TipoWait.PorTiempo) //Se espera el tiempo indicado
            {
                try
                {
                    wait.Until(driver => (DateTime.Now - timeIni) > delay);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
        }
        //Se espera hasta que el nombre del usuario coincida con el de la fila de la tabla correspondiente
        public void WaitForNombre(int seg, string nombre, int filaTabla, int timeOut = 5)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 0, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeIni = DateTime.Now;
            wait.Until(webDriver => driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[1]")).Text == nombre);
        }
        // Se espera hasta que el formulario se rellene con los par�metros seleccionados
        public void WaitForCampos(int seg, string nombre, string edad, string altura, bool activo, int filaTabla, int timeOut = 5)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 0, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeIni = DateTime.Now;
            try
            {
                wait.Until(webDriver =>
                                driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[1]")).Text == nombre &&
                                driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[2]")).Text == edad &&
                                driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[3]")).Text == altura &&
                                bool.Parse(driver.FindElement(By.XPath("//*[@id='tbody-usuarios']/tr[" + filaTabla + "]/td[4]")).Text) == activo
                            );
            }
            catch (Exception ex)
            {

            }

        }
    }
}