﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    [ApiController]
    [Route("[controller]")]
    public class UsuariosController : ControllerBase

    {
        IModeloGenerico<Usuario> modeloUsuario;

        
        private readonly ILogger<UsuariosController> _logger;
        public UsuariosController(ILogger<UsuariosController> logger )
        {
            modeloUsuario = ModeloUsuario.Instancia;
            _logger = logger;
        }
/*
        public void Crear(Usuario nuevoObj)
        {
            modeloUsuario.Crear(nuevoObj);
        }
        
        public IList<Usuario> LeerTodos()
        {
            return modeloUsuario.LeerTodos();
        }
        
        

        public bool Eliminar(int entero)
        {
            return modeloUsuario.Eliminar(entero);
        }

        public Usuario LeerUno(int entero)
        {
            return modeloUsuario.LeerUno(entero);
        }
        public bool LeerConfirmar(string nombre)
        {
            return modeloUsuario.LeerConfirmar(nombre);
        }

        public void Modificar(Usuario usuario)
        {
            modeloUsuario.Modificar(usuario);
        }*/

        [HttpGet]
        public IList<Usuario> GetLeerTodos()
        {
            IList<Usuario> lista = modeloUsuario.LeerTodos();
            return lista;
        }


    
        [HttpGet("{idUsuario}")]
        public Usuario GetLeerUno(int idUsuario)
        {
            int? posicionUsu = null;
            Usuario usuario;
            for (int i = 0; i < modeloUsuario.LeerTodos().Count; i ++)
            {
                if (modeloUsuario.LeerTodos()[i].Id == idUsuario)
                {
                    posicionUsu = i;
                }
            }
            try
            {
                usuario = modeloUsuario.LeerUno((int)posicionUsu);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException("No existe el usuario");
            }
            return usuario;
        }
        [HttpPost]
        public Usuario CrearUno([FromBody]Usuario usuario)
        {
            return  modeloUsuario.Crear(usuario);
        }

        [HttpPut]
        public Usuario ModificarUno([FromBody] Usuario usuario)
        {
            return modeloUsuario.Modificar(usuario);
        }

        [HttpDelete("{idUsuario}")]
        public string DeleteUno(int idUsuario)
        {
            if (modeloUsuario.Eliminar(idUsuario))
            {
                return "Eliminado";

            } else
            {
                return "No eliminado";

            }
        }
    }
}
